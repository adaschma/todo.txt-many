{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
  nativeBuildInputs = with pkgs.buildPackages; [
    gnumake
    mypy
    docutils
    python311Packages.flake8
    #clitest   # not in nix
  ];
}
