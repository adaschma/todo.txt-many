Run all tests via `make test` in the top most project directory.


Integration tests
=================

The most important integration tests are in the main user documentation. Here
we only include cases that are not useful there.

We want to show a dedicated error message when we get an invalid input.

.. code::

    $ todo-txt many pri b
    Error: No ITEM#-Expression found.
    $ todo-txt many pri 1 b
    Error: No ITEM#-Expression found.
