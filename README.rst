=============
todo-txt many
=============

Regularly when I use `todo-txt`_, I want to make the same change (e.g. change
the priority) on multiple tasks. This is an add-on to do that in a single
action.

.. _todo-txt: http://todotxt.org/

Installation
============

Clone this repository and link from your `$TODO_ACTIONS_DIR` directory to
`many`. Besides `todo-txt` itself only `python3`_ is required.

.. _python3: https://python.org/


Usage
=====

.. code::

    $ todo-txt help many
      many ACTION TERM...
        Run ACTION for multible different ITEM#s
    
Let's demonstrate how to use `many` on a short todo list:

.. code::

    $ todo-txt ls
    1 Call Mom @Phone +Family
    2 Schedule annual checkup +Health
    3 Outilne chapter 5 +Novel @Computer
    4 Add cover sheets @Office +TPSReports
    5 Download Todo.txt mobile app @Phone
    6 Pick up milk @GroceryStore
    7 Plan backyard herb garden @Home
    --
    TODO: 7 of 7 tasks shown

Now suppose we want to prioritize many different tasks. Instead of calling
`todo-txt` multiple times with the different ITEM#s `todo-txt pri ITEM# b` we
use this new `many` action before the actual action `pri` and replace the ITEM#
with a ITEM#-expression. With an example it should be clearer:

.. code::

    $ todo-txt many pri 1-3,5 b
    5 (B) Download Todo.txt mobile app @Phone
    TODO: 5 prioritized (B).
    3 (B) Outilne chapter 5 +Novel @Computer
    TODO: 3 prioritized (B).
    2 (B) Schedule annual checkup +Health
    TODO: 2 prioritized (B).
    1 (B) Call Mom @Phone +Family
    TODO: 1 prioritized (B).

The ITEM#-expression in this example is the `1-3,5`. The demonstrates that
beside listing a specific ITEM# (`5`) we can give an entire range (1 to 3
inclusive) with a `-`. And we combine multiple parts with the `,`. The result
is the same as if we would have `todo-txt pri` multiple times (because `many`
did that for us):

.. code::

    $ todo-txt ls
    1 (B) Call Mom @Phone +Family
    2 (B) Schedule annual checkup +Health
    3 (B) Outilne chapter 5 +Novel @Computer
    4 Add cover sheets @Office +TPSReports
    5 (B) Download Todo.txt mobile app @Phone
    6 Pick up milk @GroceryStore
    7 Plan backyard herb garden @Home
    --
    TODO: 7 of 7 tasks shown


Suppose we want to deprioritize all `@Phone` tasks. The way to do this with
vanilla `todo-txt` is to first list all tasks.

.. code::

    $ todo-txt ls @Phone
    1 (B) Call Mom @Phone +Family
    5 (B) Download Todo.txt mobile app @Phone
    --
    TODO: 2 of 7 tasks shown


And then manually putting the ITEM# into the `depri` subcommand. With `many`
however, we don't need to copy the ITEM# manually, but let the computer do that
for us.

.. code::

    $ todo-txt many depri /@Phone/
    5 Download Todo.txt mobile app @Phone
    TODO: 5 deprioritized.
    1 Call Mom @Phone +Family
    TODO: 1 deprioritized.

Everything between the two slashes `/` is searched just like with the `ls`
earlier and then passed to the given subcommand one-by-one. The result is what
you would expect:

.. code::

    $ todo-txt ls
    1 Call Mom @Phone +Family
    2 (B) Schedule annual checkup +Health
    3 (B) Outilne chapter 5 +Novel @Computer
    4 Add cover sheets @Office +TPSReports
    5 Download Todo.txt mobile app @Phone
    6 Pick up milk @GroceryStore
    7 Plan backyard herb garden @Home
    --
    TODO: 7 of 7 tasks shown

Note that it is currently not possible to combine searches with listing of
ITEM#-expressions. The search is passed as is to the `ls` subcommand so you can
use any of its advanced searches, too:

.. code::

    $ todo-txt many pri '/Download\|@Computer/' c
    5 (C) Download Todo.txt mobile app @Phone
    TODO: 5 prioritized (C).
    3 (C) Outilne chapter 5 +Novel @Computer
    TODO: 3 re-prioritized from (B) to (C).
    $ todo-txt many pri '/mobile app/' a
    5 (A) Download Todo.txt mobile app @Phone
    TODO: 5 re-prioritized from (C) to (A).
    $ todo-txt many pri '/Download mobile/' b
    $ todo-txt many pri /Download mobile/ c
    5 (C) Download Todo.txt mobile app @Phone
    TODO: 5 re-prioritized from (A) to (C).


`many` by default searches in the `$TODO_FILE`. But a few commands work on a
different file. In that case you must tell which file the it should search in.
Relative paths are relative to `$TODO_DIR`.


.. code::

    $ todo-txt lf routine.todo.txt
    1 Come hair +morning
    2 Eat breakfast +morning
    3 Brush teath +morning
    4 Take shower +morning
    5 Brush teath +night
    6 Wash face +night
    --
    ROUTINE: 6 of 6 tasks shown
    $ TODO_MANY_SEARCH_FILE=routine.todo.txt todo-txt many -f mv /+night/ todo.txt routine.todo.txt > /dev/null
    $ todo-txt ls
    1 Call Mom @Phone +Family
    2 (B) Schedule annual checkup +Health
    3 (C) Outilne chapter 5 +Novel @Computer
    4 Add cover sheets @Office +TPSReports
    5 (C) Download Todo.txt mobile app @Phone
    6 Pick up milk @GroceryStore
    7 Plan backyard herb garden @Home
    8 Wash face +night
    9 Brush teath +night
    --
    TODO: 9 of 9 tasks shown

