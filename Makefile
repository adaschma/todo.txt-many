
test:
	mypy --strict many
	./many --test
	for file in README.rst tests/README.rst; do \
		clitest --prefix '    ' \
			--pre-flight "cp tests/setup/todo.txt.orig tests/setup/todo.txt; cp tests/setup/routine.todo.txt.orig tests/setup/routine.todo.txt; alias todo-txt='todo-txt -d tests/setup/todo.cfg -p'" \
			$$file; \
		rst2html $$file /dev/null; \
	done
	flake8 many
